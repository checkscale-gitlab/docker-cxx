# Docker Cxx

A Docker image for C++ development

## Contains

* [Git](https://git-scm.com)
* [LCOV](http://ltp.sourceforge.net/coverage/lcov.php)
* [Bash](https://www.gnu.org/software/bash/)
* [Make](https://www.gnu.org/software/make/)
* [Ninja](https://ninja-build.org)
* [CMake](https://cmake.org)
* [Doxygen](http://www.doxygen.nl)
* [GNU GCC C/C++ compiler](https://gcc.gnu.org)
* [Clang C/C++ compiler](https://clang.llvm.org)
* C/C++ standard libraries ([musl libc](https://www.musl-libc.org))
* Compiler runtime libraries ([compiler-rt](https://compiler-rt.llvm.org))
