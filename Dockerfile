# Copyright 2019 Tymoteusz Blazejczyk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

ARG ALPINE_VERSION=latest
FROM alpine:${ALPINE_VERSION} as alpine

FROM alpine as builder

ARG LCOV_VERSION=1.14

# Install the LCOV tool for a graphical front-end C/C++ code coverage
RUN apk add --quiet --no-cache \
        perl \
        make \
        bash && \
    LCOV=lcov-$LCOV_VERSION && \
    wget -P /tmp/ http://downloads.sourceforge.net/ltp/$LCOV.tar.gz && \
    tar xvf /tmp/$LCOV.tar.gz -C /tmp/ && \
    make -C /tmp/$LCOV install && \
    rm -rf /tmp/*

FROM alpine

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION

# Build-time metadata as defined at http://label-schema.org
LABEL \
    maintainer="tymoteusz.blazejczyk.pl@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="tymonx/cxx" \
    org.label-schema.description="A Docker image for C++ development" \
    org.label-schema.usage="https://gitlab.com/tymonx/docker-cxx/README.md" \
    org.label-schema.url="https://gitlab.com/tymonx/docker-cxx" \
    org.label-schema.vcs-url="https://gitlab.com/tymonx/docker-cxx" \
    org.label-schema.vcs-ref=$VCS_REF \
    org.label-schema.vendor="tymonx" \
    org.label-schema.version=$VERSION \
    org.label-schema.docker.cmd=\
"docker run --rm --user $(id -u):$(id -g) --volume $(pwd):$(pwd) --workdir $(pwd) --entrypoint /bin/sh tymonx/cxx"

COPY --from=builder /usr/local/ /usr/local/

# Install updated packages for a C/C++ development.
# The --no-cache option doesn't store temporary cache files under the
# /var/cache/ location. Also on default it runs the 'apk update' command.
# No need to manually remove the /var/cache/* content.
# Patching Clang paths for installed 'compiler-rt-static' libraries
# Needed for generating C/C++ code coverage artifacts at runtime
# It gets a proper Clang version in a format: 'version <value>'
RUN apk add --quiet --no-cache \
        compiler-rt-static \
        musl-dev \
        perl \
        bash \
        llvm \
        git \
        gcc \
        g++ \
        clang \
        make \
        ninja \
        cmake \
        doxygen && \
    VERSION=$(clang --version | sed -n 's/.*version\s\+\(\S\+\).*/\1/p') && \
    mkdir -p /usr/lib/clang/$VERSION/lib/linux/ && \
    ln -s /usr/lib/clang/$VERSION/*.a /usr/lib/clang/$VERSION/lib/linux/
